# Django mysql connection with docker

It's  a simple example for connect django project with mysql using docker (without docker compose)

## Launch

For launch the mysql-server and django project you simply must run the run.sh file:

```bash
chmod 755 run.sh
./run.sh
```

After, visit http://172.18.1.5:8000, if you see the Django "test" page, it's working. 

## Stop and clean

For stop the containers (django and mysql) and delete the images, you must run the clean file:

```bash
chmod 755 clean.sh
./clean.sh
```

In the line 8 the instruction for delete the mysql image is commented, if you want to delete the mysql image you simply 
uncomment the line, or run:

```bash
docker rmi mysql:8
```

## Explanations

The idea of this project is connect a Django with mysql server using Docker.
To make it we use docker network

We created a network:
```docker
docker network create django-mysql-nw --subnet 172.18.1.0/16 --gateway 172.18.1.1
```

After we run the mysql image assigning the ip 172.18.1.4, also create a user, password and database for connection with
django:

```dockerfile
docker run -d --network django-mysql-nw --ip 172.18.1.4 \
  -e MYSQL_ROOT_PASSWORD=root_password123 \
  -e MYSQL_DATABASE=django \
  -e MYSQL_USER=django -e MYSQL_PASSWORD=django \
  --name=mysql_server_django mysql:8
```


To configure the database in Django, we modify the file settings.py, in this case we decided assign the params using
environment variables (the same for allowed hosts).

The configuration in settings.py:
```python
ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS").split(" ")
...
DATABASES = {
    "default": {
        "ENGINE": os.environ.get("SQL_ENGINE"),
        "NAME": os.environ.get("SQL_DATABASE"),
        "USER": os.environ.get("SQL_USER"),
        "PASSWORD": os.environ.get("SQL_PASSWORD"),
        "HOST": os.environ.get("SQL_HOST"),
        "PORT": os.environ.get("SQL_PORT"),
    }
}
```
In the file env.django we set the values: 
```
SQL_ENGINE=django.db.backends.mysql
SQL_DATABASE=django
SQL_USER=django
SQL_PASSWORD=django
SQL_HOST=172.18.1.4
SQL_PORT=3306
DJANGO_ALLOWED_HOSTS=localhost 172.18.1.5
```
We create and run the Django image with assigning the ip 172.18.1.5, the file with environment variables.
Also, we make the migrations:

```dockerfile
docker build --tag django_server_mysql:v1 .
docker run -d --network django-mysql-nw --ip 172.18.1.5 \
  --env-file ./env.django \
  --name  django_server_mysql django_server_mysql:v1

docker exec -it django_server_mysql python manage.py migrate
```
The configuration of the docker image is in de file Dockerfile. 
