FROM python:3.10.0b4-alpine3.14
# set work directory
WORKDIR ./django_project

RUN apk add musl-dev mariadb-dev gcc

#Virtualenv
ENV VIRTUAL_ENV=/opt/vedpnv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /django_project
RUN pip install -r requirements.txt

# copy project
COPY ./django_project/ /django_project
#RUN python manage.py migrate

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
