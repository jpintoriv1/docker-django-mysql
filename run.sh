docker pull mysql:8

docker network create django-mysql-nw --subnet 172.18.1.0/16 --gateway 172.18.1.1

docker run -d --network django-mysql-nw --ip 172.18.1.4 \
  -e MYSQL_ROOT_PASSWORD=root_password123 \
  -e MYSQL_DATABASE=django \
  -e MYSQL_USER=django -e MYSQL_PASSWORD=django \
  --name=mysql_server_django mysql:8

docker build --tag django_server_mysql:v1 .
docker run -d --network django-mysql-nw --ip 172.18.1.5 \
  --env-file ./env.django \
  --name  django_server_mysql django_server_mysql:v1

docker exec -it django_server_mysql python manage.py migrate